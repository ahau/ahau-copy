# ssb-copy

This module can be used to copy records between groups, or export them as backups


## API


### `const copy = new Copy(ssb)`


### `copy.add(recordId, opts, cb)`

- `recordId` *MessageId* - the root message id for a record
- `opts` *Object* (optional)
  - `opts.read(recordId, cb)` a read function which returns the current state of the record (expects a single state, not a branched state)
- `cb` *function* callback which will be passed an error if there is any problem reading the record
    - errors occur when:
        - you try to import a record with the same `recordId` but different resolved state
        - you try to import a record of unknown / unsupported type

### `copy.addTribe(groupId, cb)`

Calls `copy.add` on all known record types associated with a tribe with id `groupId`

Records copied
- Profiles: `profile/person`, `profile/community` (public and private), `link/group-profile`
- Whakapapa: `whakapapa/view`, `link/profile-profile/child`, `link/profile-profile/partner`
- Stories: `artefact/*`, `story/*`, `link/story-artefact`, `link/story-profile/contributor`, `link/story-profile/mention`
- Collections: `collection`, `link/collection-story`

### `copy.export() => data`

Exports a `data` object for your records containing all records that were added, grouped by type, then msgId.

### `copy.import(data, cb)`

Loads `data` object into the copy instance, (must be in format that was produced by `copy.export`)
Callback has signature `cb(err)`

Errors will be registered if:
- you import data and there's a conflicting record that's already been added/ imported
    - _conflict_ here means there's a record with same id that's NOT deep-equal
- you try to import a record type which is not known / supported
 
### `copy.execute(recps, cb)`

Write all records which have been added/ imported to `recps`
- `recps` *Array* an array of recipients. If copying to a group, this is just `[GroupId]`
- `cb` *function* callback function with signature `cb(err)`

### `copy.progress() => data`

Peek in to see to progress of `execute()`.
This can be used for persisting progress so that you can resume should the application be closed mid-execute

```js
{
  id: Hash,              // a unique id of the execute
  copied: [MessageId],   // list of records written (referenced by original id)
  current: 405,          // copied.length
  target: 1503,          // total number of records we expect to create
}
```

---



algorithm:
- mark records for copy
  - also adds dependancies (if not already present)
  - can list records currently marked (later)
  - can remove form list (later)
- initiate copy/ export
- record progress,
- done


storing records:

```js
{
  whakapapaView: {
    viewId: View
  },
  person: {
    profileId: Profile
  },
  children: {
    parentProfilId: childProfileId
  },


}
```

order of creation:
- 0: profile/person|community, artefact
- 1: link/group-profile, whakapapaView, link/profile-profile/child|partner, story 
- 2: link/story-artefact, link/story-profile/mention, link/story-profile/contributor
- 3: collection
- 4: link/collection-story


other:
- application?


